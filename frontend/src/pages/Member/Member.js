import React from "react";
import Dashboard from "../../components/Dashboard";
import ListTable from "../../components/elements/ListTable";

export default function Member() {
  return (
    <Dashboard>
      <h1>Hai Member</h1>
      <ListTable />
    </Dashboard>
  );
}
