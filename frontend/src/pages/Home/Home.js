import React from "react";
import Dashboard from "../../components/Dashboard";
import CountCard from "../../components/elements/CountCard";

export default function Home({ name, image, stock, price, onCart, onClick }) {
  return (
    <Dashboard>
      {/* <div className="bg-info"> */}
      <h1>Hai HOME</h1>
      <div className="card p-3 border border-primary border-3">
        {/* <div className="col-5 ">
            <img src={image} className="img" height="200" width="200" alt={name} />
          </div> */}

        <h5 className="text-dark">Nama user {name} </h5>
        <h6 className="text-danger">mode user {price}</h6>
        <h6 className="text-dark">nama outlet{stock}</h6>

        {/* button untuk menambah ke keranjang belanja */
        /* <button className="btn btn-sm btn-success m-1" onClick={onCart}>
                Tambahkan ke keranjang belanja
              </button> */
        /* </div> */
        /* </div> */}
      </div>
      <CountCard />

      {/* </div> */}
    </Dashboard>
  );
}
