import { getAllUsers, putDataUser, deleteUser } from "../../utils/fetch-2";

export function getAll(page, setResponse) {
  getAllUsers(page)
    .then((res) => {
      if (res.success) {
        return setResponse({
          success: true,
          message: "SUKSES",
          data: res.data,
          meta: res.meta,
        });
      } else {
        console.log(res.message);
        return setResponse({ success: false, message: res.message });
      }
    })
    .catch((err) => {
      console.log(err);
      return setResponse({
        success: false,
        message: "Silahkan Hubungi Developer",
      });
    });
}
