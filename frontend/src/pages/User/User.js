import React, { useState } from "react";
import Dashboard from "../../components/Dashboard";
import ListTable from "../../components/elements/ListTable";
import { getAll, editUser, deleteOne } from "./action";

export default function User() {
  // deklarasi variabel di function component
  const [count, setCount] = useState(0);

  // useEffect seperti componentDidMount dan componentDidUpdate
  // useEffect(() => {
  //    memperbarui judul dokumen menggunakan API browser
  //   if (response.message === 'token tidak valid') history.push('/login');
  // }, [response]);

  return (
    <Dashboard>
      <h1>Hai KARYAWAN</h1>
      <ListTable />
      <div>
        <p>You clicked {count} times</p>
        <button onClick={() => setCount(count + 1)}>Click me</button>
      </div>
    </Dashboard>
  );
}
