import React from "react";
import PropTypes from "prop-types";

export default function ListTable({ name, value }) {
  return (
    <table className="table">
      {/* table-striped */}
      <thead>
        <tr className="table-warning">
          <th scope="col">#</th>
          <th scope="col">Username</th>
          <th scope="col">Email</th>
          {/* <th scope="col">Password</th> */}
          <th scope="col">Role</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Mark</td>
          <td>Otto</td>
          <td>@mdo</td>
          <td>
            <button type="button" className="btn btn-primary btn-sm me-2">
              Edit
            </button>
            <button type="button" className="btn btn-danger btn-sm">
              Delete
            </button>
          </td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Jacob</td>
          <td>Thornton</td>
          <td>Otto</td>
          <td>
            <button type="button" className="btn btn-primary btn-sm me-2">
              Primary
            </button>
            <button type="button" className="btn btn-primary btn-sm">
              Primary
            </button>
          </td>
        </tr>
      </tbody>
    </table>
  );
}
