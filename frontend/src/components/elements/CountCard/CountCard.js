import React from "react";
import PropTypes from "prop-types";
import ProgressBar from "../ProgressBar";

export default function CountCard({ name, image, stock, price, onCart, onClick }) {
  return (
    <div>
      <div className="mt-2 d-flex align-content-stretch flex-wrap">
        {/* <div className="card border border-warning border-4 me-3"> */}
        <div className="card me-3 shadow p-3 mb-5 bg-body rounded">
          <div className="row p-4">
            <div className="col-7 ">
              <h6 className="mb-1 fw-bold">Total Pesanan</h6>
              <p className="text-black-50">Paket seluruh</p>
            </div>
            <div className="col-5 ">
              <h2 className="text-success">1234%</h2>
            </div>
            <ProgressBar />
          </div>
        </div>
        {/* 2 */}
        <div className="card me-3 shadow p-3 mb-5 bg-body rounded">
          <div className="row p-4">
            <div className="col-7 ">
              <h6 className="mb-1 fw-bold">Total Pesanan</h6>
              <p className="text-black-50">Paket seluruh</p>
            </div>
            <div className="col-5 ">
              <h2 className="text-success">1234%</h2>
            </div>
            <ProgressBar />
          </div>
        </div>
        {/* 2 */}
        {/* <div className="card border border-warning border-4 me-3"> */}
        <div className="card me-3 shadow p-3 mb-5 bg-body rounded">
          <div className="row p-4">
            <div className="col-7 ">
              <h6 className="mb-1 fw-bold">Total Pesanan</h6>
              <p className="text-black-50">Paket seluruh</p>
            </div>
            <div className="col-5 ">
              <h2 className="text-success">1234%</h2>
            </div>
            <ProgressBar />
          </div>
        </div>
        {/* 2 */}
        {/* <div className="card border border-warning border-4 me-3"> */}
        <div className="card me-3 shadow p-3 mb-5 bg-body rounded">
          <div className="row p-4">
            <div className="col-7 ">
              <h6 className="mb-1 fw-bold">Total Pesanan</h6>
              <p className="text-black-50">Paket seluruh</p>
            </div>
            <div className="col-5 ">
              <h2 className="text-success">1234%</h2>
            </div>
            <ProgressBar />
          </div>
        </div>
      </div>
    </div>
  );
}

CountCard.defaultProps = {
  name: "",
  value: null,
  onClick: () => {},
};

CountCard.propTypes = {
  name: PropTypes.string,
  value: PropTypes.number,
  onClick: PropTypes.func,
};
