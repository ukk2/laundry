import React from "react";
import PropTypes from "prop-types";

export default function ListCard({ name, image, stock, price, onCart, onClick }) {
  return (
    <div className="bg-info d-flex align-content-stretch flex-wrap">
      {/* <div class="p-2 bg-danger col-4">
        <div className="card">
          <img src={image} className="img" height="200" width="100%" alt={name} />
          <div className="card-body bg-warning">
            <h5 className="card-title">Cuci Selimut</h5>
            <p className="card-text">Cuci - Kering - Setrika - Lipat - Packing</p>
            <button href="#" className="btn btn-primary">
              Buat Pesanan
            </button>
          </div>
        </div>
      </div> */}
      <div class="p-2 bg-danger col-4">
        <div className="card">
          <img src={image} className="img" height="200" width="100%" alt={name} />
          <div className="card-body bg-warning">
            <h5 className="card-title">Cuci Selimut</h5>
            <p className="card-text">Cuci - Kering - Setrika - Lipat - Packing</p>
            <button href="#" className="btn btn-primary">
              Buat Pesanan
            </button>
          </div>
        </div>
      </div>
      <div class="p-2 bg-danger col-4">
        <div className="card">
          <img src={image} className="img" height="200" width="100%" alt={name} />
          <div className="card-body bg-warning">
            <h5 className="card-title">Cuci Selimut</h5>
            <p className="card-text">Cuci - Kering - Setrika - Lipat - Packing</p>
            <button href="#" className="btn btn-primary">
              Buat Pesanan
            </button>
          </div>
        </div>
      </div>
      <div class="p-2 bg-danger col-4">
        <div className="card">
          <img src={image} className="img" height="200" width="100%" alt={name} />
          <div className="card-body bg-warning">
            <h5 className="card-title">Cuci Selimut</h5>
            <p className="card-text">Cuci - Kering - Setrika - Lipat - Packing</p>
            <button href="#" className="btn btn-primary">
              Buat Pesanan
            </button>
          </div>
        </div>
      </div>
      <div class="p-2 bg-danger col-4">
        <div className="card">
          <img src={image} className="img" height="200" width="100%" alt={name} />
          <div className="card-body bg-warning">
            <h5 className="card-title">Cuci Selimut</h5>
            <p className="card-text">Cuci - Kering - Setrika - Lipat - Packing</p>
            <button href="#" className="btn btn-primary">
              Buat Pesanan
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

ListCard.defaultProps = {
  name: "",
  value: null,
  onClick: () => {},
};

ListCard.propTypes = {
  name: PropTypes.string,
  value: PropTypes.number,
  onClick: PropTypes.func,
};
