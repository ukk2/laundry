// Fetch.js berisi semua sambungan fe - be

import axios from "axios";
import { getToken, clearStorage } from "./storage";

// utk get token dari localStorage
export function BEARER_AUTH() {
  return { Authorization: `${getToken()}` };
}

const BASE_URL = "http://localhost:8080/api";

// proses authentifikasi dan authorize (?)
//  fetch adalah function = inputan yang pertama jadi url, kedua method, ketiga sama keempat param
const fetch = (url, method, param1, param2) => {
  return new Promise((resolve, reject) => {
    // axios adl object yg isinya function -> jadi axios.post(url, param...)
    axios[method](url, param1, param2)
      .then((res) => {
        //cek jika token valid atau tidak
        //jika tidak valid maka storage yang berisi token akan dibersihkan
        //jika storage kosong maka akan langsung kembali ke login (useEffect di App.js)
        if (res.data.message === "token tidak valid") clearStorage();
        //Lainya, langsung dikasih res.data
        else resolve(res.data);
      })
      .catch((err) => {
        //jika dapet error
        const defaultError = {
          code: 500,
          status: "error",
          message: "Failed to fetch data. Please contact developer.",
        };
        if (!err.message) reject(defaultError);
        else reject(err);
      });
  });
};

// Tempat Fecth dibawah ini

// buat login
// export const loginAdmin = async (data) => await fetch(`${BASE_URL}/login`, "post", data);

// Ambil data user
// export const getUser = async () => await fetch(`${BASE_URL}/user`, "get", { headers: BEARER_AUTH() });

// Buat halaman User
export const getAllUsers = async (page) =>
  await fetch(`${BASE_URL}/user?page=${page}`, "get", {
    headers: BEARER_AUTH(),
  });

// export const putDataUser = async (id, data) =>
//   await fetch(`${BASE_URL}/user/${id}`, "put", data, {
//     headers: BEARER_AUTH(),
//   });

// export const deleteUser = async (id) =>
//   await fetch(`${BASE_URL}/user/${id}`, "delete", {
//     headers: BEARER_AUTH(),
//   });
